/*
git add html/*
git add latex/*
*/

/* vector syntax */
unsigned const int length = 1000000;

// one way
unsigned cycle_length(...) {
    using namespace std;
    vector<unsigned> cache(length);

    cache.at(num-1) = 3;
    int j = cache.at(num-1);
}

// other way
() {
    std::vector<unsigned> cache(length);
    
}

// least preferred
using namespace std;
() {
    vector<unsigned> cache(length);
}

// 0 0 0 0 ... 0 

std::array<unsigned> cache(length);
cache[num]

/*

*/
git add . 
git commit -m "implemented X Y Z - fixes issue #3"


/*
HackerRank input format

i j -> i j k

"You can assume .... 32-bit integer" -> k can be stored in an int

i = 980,304
j = 999,306

1 <= i, j <= 1M

tst-case.in
==========
....
i j 
...

outfile
=======
....
i j k
....
=======
i = 980304
j = 999306
k = 293785

Does the fact that k can fit in an int (32-bits) necessarily imply that 
every step you do to compute k can also fit in an int (32-bits)?

int         32 bit
long -      32 bit
long long - 64 bit
*/