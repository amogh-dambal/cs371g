/**
 * Some instructions on what is/isn't valid on using on Project 1 - Collatz.
 * */

void f() {
    int* a = new int[10];   // invalid
    int a[10];              // valid

    a[0] = 1;
    a[1] = 2;

    /*
     * 2 ways to use vectors (roughly equivalent to Java's ArrayList or Python's list in C++
     * */

    using namespace std;
    vector<int> v;         // empty vector
    v.push_back(1);
    v.push_back(2);

    // do something
    vector<int> v(2);      // vector with 2 elements initially
    v[0] = 1;
    v[1] = 2;

    v.at(0) = 1;
}

/*
 * Docker command after setting it up to get access to the image for the classs
 * */
/*
docker pull gpdowning
*/


/**
 * A simple git workflow 
 * */
/*
git add .
git commit -m "fixed overflow bug"
git push                                // triggers GitLab CI
*/
