# Project #1 - Collatz Autopsy
## Introduction
This is a collection of notes (nitpicks, mostly :D) that I've come up with after grading Project #1 - Collatz. It contains some of the common errors/bad practices I've seen. The format of this document will be like this: I'll explain the issue, why it's an issue, and then provide better C++ practices.

Note: this is all sourced from both my own readings on CPP and various threads/pieces of advice I've seen over the years. If there's something in here you disagree with or that you think is wrong, please let me know ASAP!

## Table of Contents
- [Global Variables](#global-variables)
- [Program Constants](#program-constants)
- [Magic Numbers](#magic-numbers)
- [On Comments](#on-comments)
- [The Standard Namespace](#the-standard-namespace)
- [The STL](#the-stl)
- [Unit Testing](#unit-testing)
- [GitLab Issues](#gitlab-issues)
- [Conclusion](#conclusion)

## Global Variables
### The Issue
This was very prevalent in cache implementations; I saw several cases where the cache itself was a global variable shared throughout the `Collatz.cpp` file, like follows:
```cpp
#include <iostream>
...
unsigned cache[100000];				// bad practice
std::array<unsigned, 10000> cache;  // bad practice
...
int main() {
	...
	return 0;
}
```
### But Why Not?
There's nothing inherently wrong with this implementation. However, _in general_ you want to avoid using global variables - unless you have to - for a multitude of reasons: they're not thread safe, they can pollute the namespace of your project, it can make it more difficult to test, etc. 
### The Alternative
There are two alternatives. The first is to create a `CollatzSolver` class and put all of your methods in there. Then, in your main method and in your unit tests, you'd instantiate this class. 
On the other hand, you could add extra parameters to your functions that pass around a cache by reference (`const` and non-`const` as needed, of course). You would then instantiate this cache locally at the minimally required scope (for instance, in the `main()` method of `RunCollatz.cpp` if that's what is demanded by your implementation)

Solution 1:
```cpp
<file: Collatz.hpp>
...
class CollatzSolver
{
public:
	...
	unsigned max_cycle_length(unsigned, unsigned);
	...
private:
	const int CACHE_SIZE = 100000;
	std::array<int, CACHE_SIZE> cache;
};
...
<file: RunCollatz.cpp>
int main()
{
	CollatzSolver cs;
	
	while (getline(cin, s))
	{
		...
		unsigned k = cs.max_cycle_length(i, j);
	}
	...
}
```

Solution 2:
```cpp
<file: Collatz.cpp>
...
unsigned max_cycle_length(unsigned, unsigned, std::array<int, CACHE_SIZE>&)
{
	...
}
```

## Program Constants
### The Issue
A lot of y'all used either `#define` or a `const` global for program constants (like cache size or meta-cache chunk size), as follows:
```cpp
#define CACHE_SIZE 10000 				// wrong!
const unsigned CACHE_SIZE = 10000;		// better, but still not good
```
### But Why Not?
We've already covered why not to use global variables in the previous item. However, I would also recommend avoiding the use of `#define` for program constants. The biggest reason is that a `#define` and other such macros are find-and-replace operations done by the preprocessor, which means that the symbol you're defining is not treated as if it's part of the language. A better, more in-depth explanation can be found in Scott Meyer's _Effective Modern C++_ or _Effective C++_, and a link to that particular section is here: https://flylib.com/books/en/2.887.1.13/1/
### The Alternative
Use a namespace-defined constant like so: 
```cpp
<file: Collatz.hpp>
namespace Collatz
{
	// selected this size for XYZ reason
	const int CACHE_SIZE = 1000000;
}
```
Then, when you use it, you can simply say things like `std::array<unsigned, Collatz::CACHE_SIZE>`. This way, you organize all of your variables. However, if you use the class-based solution as seen in the previous item, you won't need global constants and so can avoid this issue entirely.
## Magic Numbers
### The Issue
This is related to the previous issue, but another problem I saw was that instead of defining a variable or macro for things like cache size or chunk size or anything else, several of you simply used a raw number as follows:
```cpp
unsigned cache[100000]; 			// wrong!
```
### But Why Not?
This is what is known as a **magic number**, because its purpose and meaning are mysterious and hidden. No one knows why it is set at that specific value - it just "works" like magic. 

Though magic is cool in many, many use cases, it's something you want to avoid when writing software. The main reason(s) are related to communicability. Martin Fowler wrote that "anyone can write code that computers can read. A good programmer writes code that humans can read" and I think that's a maxim to strive towards. 

You want not only other people on your team but also future you to be able to understand your design decisions; magic numbers fail to communicate those decisions (or anything else) effectively. You also run the risk of having to change numbers in many, many places if at some point you realize that your magic numbers need updating.
### The Alternative
Use the namespace-defined constant technique as shown above.
## On Comments
### The Issue
I saw many inline comments explaining _how_ something worked or _what_ something did as opposed to **why **a particular design decision was made.
```cpp
...
/*
 * an example of less-than-ideal commenting in the function below
 */
unsigned max_cycle_length(unsigned i, unsigned j)
{
	...
	// read line by line
	while (getline(cin, s))
	{
		... 
		// odd case
		if (x & 1)
		{
			...
		}
		// even case
		else
		{

		}
	}
	
}
```
### But Why Not?
Some schools of thought say that comments themselves are a code smell, and that code should always be self-documenting. Though that's a bit extreme, I think the larger point is valid. Your code should be written cleanly and elegantly - with good, concise structure and good variable names - so that it's not necessary to have comments explaining what the code is doing at every step of the way. The _code_ should be doing that explaining for you - if not, you might need to refactor it. 
### The Alternative
Your comments should be very, very judiciously written. Use them to explain **why** you made a particular design choice, or use them if you're using some clever bitshift trick or some weird algorithmic hack that might be confusing.

For example, one place that I would have loved to see comments in Collatz is in explaining the size of either the cache or the chunks used by the meta-cache. Why did you choose that particular size for the cache? Why did you choose that particular chunk size? These are things you should think about commenting, because they make your design and your code more understandable to you and your team.
## The Standard Namespace
### The Issue
Writing the `using namespace std;` directive at the global scope, like so: 
```cpp
#include <iostream>
...

using namespace std;		// avoid this!

int main()
{
	...
	return 0;
}
```
### But Why Not?
This is generally considered bad practice because of namespace collisions. The standard namespace is MASSIVE, and contains a ton of identifiers that at some point you are going to end up using in your program. At that point, you will want to have some way to distinguish between the `std::blah` and your `blah`. See this StackOverflow thread for a more well written thread on why not to have a global directive like that: https://stackoverflow.com/questions/1452721/why-is-using-namespace-std-considered-bad-practice
### The Alternative
Here are the alternatives, which I'll list in decreasing order of preference.

1) Qualifying the namespace
```cpp
#include <iostream>
#include <array>

...
int main()
{
	std::cout << "blah" << std::endl;
	...
	std::array<int, 10> arr;			// yes I know no magic numbers
}

```
2) Qualifying the directive 
```cpp
#include <iostream>

using std::cout;
using std::endl;
...
int main()
{
	cout << "Done." << endl; 
}
```
3) Importing the namespace at the minimally required scope
```cpp
#include <iostream>

int main()
{
	using namespace std;
	
	cout << "Done." << endl;
}
```
Any of the above is preferred to having the `using` directive at global scope.

## The STL
### The Issue
Using native C arrays or any native C construct when it would have been better and cleaner to use the STL. For example, 
```cpp
...
unsigned cache[CACHE_SIZE]; 			// bad! don't use native C arrays
```
### But Why?
The biggest reason to avoid native C constructs as much as possible is to protect your own sanity while debugging. Consider what happens when you access an index that is out-of-bounds on a native C array:
```cpp
int main()
{
	int arr[10];
	arr[11] = 3; 	// segmentation fault!
}
```
However, in a project where you have no less than 10 different places where you're reading/writing memory, it can be ridiculously frustrating to try and debug exactly where you are segfaulting. That is the problem with C arrays - **not only are they more prone to crash, but the error messages they provide when they crash are less helpful**. Debugging a segmentation fault is something you want to avoid at all costs. 

On the other hand, consider what happens when you use the STL correctly:
```cpp
int main()
{
	std::array<int, 10> arr;
	arr.at(11) = 3;	// exception!
}
```
In this instance, you'll get an `std::out_of_bounds` exception, which is exponentially easier to debug and understand. The program is telling you EXACTLY what's going on - you're accessing an element out of bounds.

**Note:** you have to (and should) use the STL container's `at()` method instead of the more typical `operator[]`, since the bracket notation will not throw the exception. 

The other major reason for preferring the STL is that not only does `std::array` along with other container have value semantics (meaning you can copy containers and treat them like primitive values), but they also fit more easily within various STL algorithms - for example, almost all of the algorithms in the `<algorithm>` header - that you should be making use of to simplify and optimize your code. 
## Unit Testing
### The Issue
Poorly or non-labeled unit tests, unit tests that don't test every unit.
### But Why Not?
Let's discuss the latter first. When you write unit tests, you want to be sure that you are breaking your program down into the smallest possible blocks and writing tests for each block (i.e. function) that you've created. In the Collatz project, for example, one particularly easy way to do this was to write a function `calculate_cycle_length(unsigned)` that would return the cycle length of any integer. Then, you could write unit tests for that function to ensure that your computation of the cycle length was solid before moving on to other parts of the code. 
Not only does this framework encourage breaking code up into multiple functions, it also helps you to isolate bugs and failures when they do happen, because you'll have extensively tested all the various interacting units of your program in order to isolate what's failing at a particular moment.

Now, suppose you have such a well-tested program, but you have no test labels. When you make a change that breaks a couple of unit tests, it will be helpful to have labels - either directly through the Google Test framework by naming the tests/fixtures and using tags or by writing comments detailing what's being tested at each test case - to help you figure out exactly what each test does, so if it breaks, you'll know what exactly is going wrong.
### The Alternative
1) Break up your code into functions. A good rule of thumb is that you should be able to describe the behavior of the function in one short clause. If you end up saying "and" while talking about what your function does, it does too many things.
2) Write exhaustive unit tests for each function, so you can isolate bad behavior in your program
3) Label those unit tests so that when you add new features later that break your code (because there will always be something that breaks the tests) you know what exact functionality is broken.

## GitLab Issues
### The Issue
Not using GitLab issue labeling/lists and not using GitLab commit messages to close issues automatically
### But Why Not?
Honestly, if you don't want to use GitLab commit messages to close issues you don't have to. I just found that it makes it a lot easier to trace what commit did what, so if a commit ever breaks something you can easily backtrack and figure things out.

However, in terms of labeling your issues, you have the option to do so when creating your issues and I highly recommend doing so. It helps you sort and organize your issues by category and can help you prioritize what to work on. It is also good practice to get into, since I believe issue tracking is industry standard. 
### The Alternative
You can add labels when you create issues in GitLab. 

To close a particular issue via the commit message, just type in something like:
`git commit -m "changed XYZ - fixes issue #N"` to close issue number `N`

## Conclusion
This was a long document, but I hope you found something useful you could take from it!

If you disagree with anything above, or have a suggestion for something to add to it, please message me and I'll take care of it! -Amogh