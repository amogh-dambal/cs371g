 # A Guide on Acceptance Tests
 So you've written all of your acceptance tests and run `checktestdata` on them, and they pass! Now you need to add them to the public test repo. Here's how to go about doing that. 

Step 1: **Fork** the public `cs371g-collatz-tests` repository, found at: https://gitlab.com/gpdowning/cs371g-collatz-tests. 
![](img1.png)

Step 2: Select the namespace matching your **GitLab ID** in which to fork the project. (mine says Amogh Dambal, yours will be - hopefully - whatever your **GitLab ID** is) and click _Go to Project_

![](img2.png)

Step 3: This is now your project, forked from the main repository. Now, you can go ahead and clone the project onto your local machine and do whatever you'd like. Ideally, you'll at some point make a commit with your acceptance test files into this machine.
**Note!** The format of the acceptance test files should be as follows: 
`<GitLabID>-RunCollatz.in`, `<GitLabID>-RunCollatz.out`
So my acceptance tests would be `amogh-dambal-RunCollatz.in`, `amogh-dambal-RunCollatz.out`. 
Your acceptance tests should also pass the `checktestdata` schema. 

Step 4: So at this point, you have a copy of the global repository with your tests added. Now you're ready to make your merge request! Go to the left sidebar and click the _Merge Requests_ option. Select _New Merge Request_ from the button that appears.
![](img3.png)

Step 5: When GitLab allows you to _Select source branch_, select the `master` branch of your fork and make sure the target branch is `gpdowning/cs371g-collatz-tests master`. Click _Compare branches and continue_

Step 6: Give your merge request a relatively descriptive title and description, then scroll to the bottom and click _Create Merge Request_. 

That should be it! All that's left is for me (Amogh) to review your merge request to make sure that everything conforms to the requirements. If it does, your merge request will be accepted. Otherwise, it'll be rejected and I'll give you the reason why. 