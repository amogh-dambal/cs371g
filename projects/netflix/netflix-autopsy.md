# Project #2 - Netflix Autopsy
## Introduction
This is a collection of notes (nitpicks, mostly :D) that I've come up with after grading Project #2 - Netflix. It contains some of the common errors/bad practices I've seen. The format of this document will be like this: I'll explain the issue, why it's an issue, and then provide better C++ practices.

Note: this is all sourced from both my own readings on CPP and various threads/pieces of advice I've seen over the years. If there's something in here you disagree with or that you think is wrong, please let me know ASAP!

## Table of Contents
- [Casting](#casting)
- [STL Containers](#stl-containers)
- [Magic Numbers](#magic-numbers)
- [Passing Arguments](#passing-arguments)
- [The Map Data Structure](#the-map-data-structure)
- [Line Length](#line-length)
- [Unit Testing](#unit-testing)
- [Iteration](#iteration)
- [File Names](#file-names)
- [Conclusion](#conclusion)

## Casting
### The Issue
In this project, there were a lot of instances where you had to cast a value from one type to another. Most - if not all - of you utilized the following C-style casting technique:
```cpp
...
for (auto& p : my_map)
{
	...
	int x = (int) p.second;
	...
}
```
### But Why Not?
In general, casting is not something you want to be doing often in your programs. Casting values is often referred to as a code smell. If you are casting types - and therefore violating or in some way changing the strict typing of the C++ language - then there are other, bigger problems lurking in your code that ought to be fixed. 

However, in some cases - like in this project - where you have to cast and there's no getting around it. Even in those cases, however, you want to avoid C-style casts because they are more difficult to search for and identify in your code (either via `grep` or manually) and are less likely to behave in the way that you'd like.
### The Alternative
C++ provides 4 types of casts, and it's incredibly likely that one of them will better fit your use case and result in cleaner code than a C-style cast. They are:
1. `const_cast<T>(expression)`: used to cast away the `const`-ness of objects, its the only C++ cast that does this
2. `dynamic_cast<T>(expression)`: used to determine whether an object is in a certain C++ inheritance hierarchies. don't worry about it unless you're using inheritance
3. `reinterpret_cast<T>(expression)`: used for low-level stuff. if you're not really in the guts of the machine, don't worry about this one either.
4. `static_cast<T>(expression)`: forces implicit conversions, probably the one you're going to use most often if you want to cast something like you would in C or Java

These casts are easier to see and are more specific about what they do. So, in the case of the above example, the better way to write that code would be as follows:
```cpp
...
for (const pair<type1, type2>& p : my_map)
{
	...
	int x = static_cast<int>(p.second);
	...
}
```

This excerpt was adapted from Scott Meyers _Effective C++_, linked here: https://www.aristeia.com/EC3E/3E_item27.pdf

## STL Containers
### The Issue
One thing that I saw often was to count the number of items in an STL container by doing something like this:
```cpp
void f(const std::vector<int>& predictions)
{
	/// hmmmmmm....
	...
	int num_elements = 0;
	for (...)
	{
		...
		num_elements++;
		...
	}
	...
}
```
### But Why Not?
There's nothing wrong with this implementation (and I'm actually about 40% sure this would get optimized out by a compiler anyway), but every STL container provides you with a `size()` method - use it! It'll save you an operation in every iteration of the loop and is simply more cohesive with the code overall.
### The Alternative 
```cpp
<file: Collatz.hpp>
void f(const std::vector<int>& predictions)
{
	....
	size_t num_elements = predictions.size();
}
```
## Magic Numbers
### The Issue
This is related to the previous issue, but another problem I saw was that instead of defining a variable or macro for things like cache size or chunk size or anything else, several of you simply used a raw number as follows:
```cpp
unsigned cache[100000]; 			// wrong!
```
### But Why Not?
This is what is known as a **magic number**, because its purpose and meaning are mysterious and hidden. No one knows why it is set at that specific value - it just "works" like magic. 

Though magic is cool in many, many use cases, it's something you want to avoid when writing software. The main reason(s) are related to communicability. Martin Fowler wrote that "anyone can write code that computers can read. A good programmer writes code that humans can read" and I think that's a maxim to strive towards. 

You want not only other people on your team but also future you to be able to understand your design decisions; magic numbers fail to communicate those decisions (or anything else) effectively. You also run the risk of having to change numbers in many, many places if at some point you realize that your magic numbers need updating.
### The Alternative
Use the namespace-defined constant technique as shown above.
## Passing Arguments
### The Issue
In many of your functions and methods, you had to take an object - usually an STL container - as an argument or parameter to that function/method. However, many of you did something like the following:
```cpp
using map_type = std::unordered_map<int, char>
void f(map_type v) 		
{
	...
}

void g(map_type& v)
```
### But Why Not?
Each function above is passing arguments differently - one by **value**, one by **reference**. Let's cover each case individually. 

**Pass-by-value**
This is what is happening in  `f()`. The argument `v` is being passed **by value**, which means that a copy of `v` will be made. You generally want to **avoid passing by value**, especially if you don't NEED to make a copy of the object. I's incredibly expensive to copy these objects, especially as they get larger. In Netflix, many of these objects could be caches containing several hundreds or even thousands of records. **You generally do not want to pass pretty much any object in Netflix (or in any CS371G project) by value, ever. **

Generally, the only parameters you should pass by value are primitive (built-in) types, iterators, and functors (function objects). You may also want to pass by value if your function is going to have to create a copy of the object _anway_, since you can then make the copying of the parameter clear to the caller. The advent of move semantics in C++11 has created some more interesting cases where you may want to pass by value if your function takes ownership of the object(s) in question.

**Pass-by-reference**
This is what's happening in `g()`. Like we covered in class, we're now passing a reference to `v`. However, this can be dangerous if `g()` doesn't - or rather shouldn't- modify `v` at all, as there's nothing stopping you from accidentally changing the state of `v` when you shouldn't. 

The main takeaway is that **you only want to pass by reference if you intend on modifying the object** (and if you don't want the parameter value to be `NULL`). 
### The Alternative
In Netflix, the correct idiom of argument passing almost always was to pass by **const-reference**, since you don't intend on modifying the object or the parameter but you also don't want to incur the overhead of copying objects of that size. An example is shown below:
```cpp
using map_type = std::unordered_map<int, char>
void h(const map_type& v) // yay!
{
	...
	return;
}
```
## The Map Data Structure
### The Issue
Using an incorrect - depending on the use case - form of access into an `std::unordered_map<T>`. In particular, when trying to look for an element in your cache, several of you did something like this: 
```cpp
...
void f()
{
	using map_type = std::unordered_map<int, char>;
	map_type m
	...
	m[...] = ...;
}
```
### But Why Not?
For the problem(s) with access to become clear, it has to be made clear that there are **4** types of access into an `std::unordered_map` (note that this also applies for `std::map`, which is ordered). Each kind of access has different semantics and is meant to be used in a particular context. 
1. `operator[]`: if the element exists, it will return that element. **if it does not exist, it will create it.**
2. `at(const key_type& k)`: will return a reference to the element if it exists and will **throw an exception otherwise**
3. `find(const key_type& k)`: return a `unordered_map::iterator` to that element if it exists, an iterator to `::end` if it doesn't
4. `count(const key_type& k)`: returns the number of those elements in the map. thus, will be 0 if it exists or 1 if it doesn't

Thus, the issue was that many of you used `operator[]` to access the cache when that was not the intended behavior. I don't think that you want to add to the cache if a particular value doesn't exist - if you did, then that has to be made clear before using `operator[]`. 
### The Alternative
Overall, determine the context of your access before choosing which access type to use. If you are guaranteed that the element will be there, then prefer `.at()`. Otherwise, prefer `find()`. Only use the `operator[]` if you explicitly want to create the element if it doesn't exist, and `count()` if you're in a counting context.

## Line Length
### The Issue
Many of you had lines that were significantly longer than 80 characters, which is the ISO/ANSI standard.
### But Why?
The biggest issue is readability. The 80 character limit itself seems arbitrary now, and in fact comes from the era of IBM punchcards (which had 80 columns). That influenced the development of the earliest terminal screens, which were also 80x24. However, it's used now mostly as adherence to standard for tradition's sake.

However, it's worth noting that even if _80_ isn't your favorite number, it's still a good ballpark estimate to use when determining when to use line breaks to split up a specific line into chunks. You want to avoid a horizontal scroll bar at all costs. Barring cases where you have to print error messages to the user or something else that needs to be `grep`'d for, the presence of a line longer than 80 characters is almost certainly a code smell. 
### The Alternative
Your IDE (whether that's VSCode, CLion, Eclipse, or anything else) should have formatting options that will allow you to stick to this (or any other limit) that you choose. Whatever you end up choosing, make sure your line lengths are reasonable and ensure there's no horizontal scrolling.
## Unit Testing
### The Issue
Labeling of unit tests. 
### But Why Not?
When you make a change that breaks a couple of unit tests, it will be helpful to have labels - either directly through the Google Test framework by naming the tests/fixtures and using tags or by **writing comments detailing what's being tested** at each test case - to help you figure out exactly what each test does, so if it breaks, you'll know what exactly is going wrong.

### The Alternative
Here's an example for how to label your unit tests: 
```cpp
<file: TestInteger.hpp>
#include "gtest/gtest.h"
...
/**
 * Tests whether shift_left_digits can handle shifting by 0
 */
TEST(IntegerFixture, shift_left_zero)
{
	...
}

/**
 * Tests whether shift_left_digits can handle shifting by a
 * number greater than the number of digits in the integer.
 */
TEST(IntegerFixture, shift_left_big)
{
	...
}
```

## Iteration
### The Issue
Iterating through STL containers in the following way:
```cpp
void f(const std::vector<int>& v)
{
	for (int i = 0; i < v.size(); i++) 
	{
		... 
	}
	...
}
```
### But Why Not?
Like many of the above issues, there's nothing fundamentally _incorrect_ about iterating through an STL container like this. However, this can often lead to several issues with invalidation of the underlying container (if you ever end up modifying the container in the loop). It also restricts your code to only work with `std::vector<T>`. If you realize at some point you need to change containers, you'll have to rewrite a significant portion of your code. 

Additionally, the above code calls `size()` at every iteration of the loop, which could have unseen performance hits in the future. 
### The Alternative
Post C++11, there are two alternate ways to iterate through a container. You could use a construct that is very similar to Java's `for each` loop, which looks like this:
```cpp
struct Widget
{
	....
};

void f(const std::vector<int>& v)
{
	for (const int i : v)
	{
		...
	}
	
	std::vector<Widget> u;
	for (const Widget& w : u)
	{
		...
	}
}
```

If you prefer being more explicit about the iterators, you could use an approach like this:
```cpp
struct Widget
{
	...
};

void f(const std::vector<int>& v)
{
	for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++v)
	{
		...
	}
	
	// the auto keyword just means that the compiler will automatically deduce the type of that variable from the initializer
	std::vector<Widget> u;
	for (auto it = u.begin(); it != u.end(); ++it)
	{
		...
	}
}
```
## Conclusion
This was a long document, but I hope you found something useful you could take from it!

If you disagree with anything above, or have a suggestion for something to add to it, please message me and I'll take care of it! -Amogh